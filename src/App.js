import React, { Component } from "react";
import todosList from "./todos.json";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: todosList,
      text: ''
    };
  }

  // handleChange = (event) => {
  //   const createNewTodo = [...this.state] // Copys the todos object
  //   createNewTodo[event.target.name] = event.target.value

  // }
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.addTodo({
      userId: 1,
      id: new Date(),
      title: this.state.text,
      completed: false,
    });
    this.setState({text: ''})
  };

  addTodo = (todo) => {
    this.setState({
      todos: [todo, ...this.state.todos],
    });
  };

  // Where would I add these methods ?
  // Inspired an taken by multiple youtube videos but https://github.com/benawad/practical-react/tree/15_todo_list is the main one

  handleCompleted = (id) => (event) => {
    console.log(id);
    console.log(event);
    const newTodos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      } else {
        return todo;
      }
    });
    this.setState({
      todos: newTodos,
    });
  };

  // I literally just took this from the given code
  handleDeletedTodos = (id) => (event) => {
    const deletedTodos = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({ todos: deletedTodos });
  };
  // Literally the given code except the conditon changed
  handleClearCompletedTodos = (id) => (event) => {
    const clearCompletedTodos = this.state.todos.filter(
      (todo) => !todo.completed
    );
    this.setState({
      todos: clearCompletedTodos,
    });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.handleSubmit}>
            <input
              className="new-todo"
              name="text"
              value={this.state.text}
              onChange={this.handleChange}
              placeholder="What needs to be done?"
              autoFocus
            />
          </form>
        </header>
        <TodoList
          todos={this.state.todos}
          handleCompleted={this.handleCompleted}
          handleDeletedTodo={this.handleDeletedTodos}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button
            className="clear-completed"
            onClick={this.handleClearCompletedTodos(this.state.todos.id)}
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.handleCompleted(this.props.id)}
          />{" "}
          {/* props.id is called from the TodoList being mapped*/}
          <label>{this.props.title}</label>
          <button
            className="destroy"
            onClick={this.props.handleDeletedTodos(this.props.id)}
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo, i) => (
            <TodoItem
              key={i}
              title={todo.title}
              completed={todo.completed}
              handleCompleted={this.props.handleCompleted}
              handleDeletedTodos={this.props.handleDeletedTodo}
              id={todo.id}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
